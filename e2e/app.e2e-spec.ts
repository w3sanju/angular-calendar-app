import { CalendarAppPage } from './app.po';

describe('calendar-app App', () => {
  let page: CalendarAppPage;

  beforeEach(() => {
    page = new CalendarAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
